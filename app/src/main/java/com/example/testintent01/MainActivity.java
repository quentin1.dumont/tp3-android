package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void modifier(View view)
    {
        EditText editText = findViewById(R.id.edit_text);
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("text", editText.getText().toString());
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        TextView textView = findViewById(R.id.text_view);
        textView.setText(data.getStringExtra("text"));
    }

    public void valider(View view)
    {
        EditText editText = findViewById(R.id.edit_text);
        TextView textView = findViewById(R.id.text_view);
        editText.setText(textView.getText().toString());
    }
}