package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView textView = findViewById(R.id.text_view);
        textView.setText(getIntent().getStringExtra("text"));
    }

    public void reverse(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        TextView textView = findViewById(R.id.text_view);
        intent.putExtra("text", new StringBuilder(textView.getText().toString()).reverse().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    public void toUpper(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        TextView textView = findViewById(R.id.text_view);
        intent.putExtra("text", textView.getText().toString().toUpperCase());
        setResult(RESULT_OK, intent);
        finish();
    }
}